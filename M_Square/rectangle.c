#include "rectangle.h"
/*
algorithim:
make a square using the length of the smaller side and remove that square from rectangle
rinse, repeat until all thats left is a sqaure
*/


/*
finds the list of squares that fill rectangle
in: rectangle
out: square list
*/
size_t* squares(rectangle rec)
{
	size_t* list = NULL;
	size_t list_len = 0;
	if (check_init(rec)) {
		while (rec._length != rec._width) {//when both sides are equal there is one square left
				list_len++;
				void* temp = realloc(list, sizeof(int)*list_len);
				if (!temp) {
					free(list);
					exit(1);
				}
				list = (size_t*)temp;
				if (rec._length > rec._width) { //if length is bigger than next square uses width
					list[list_len - 1] = rec._width;
					rec._length -= rec._width;
				}
				else { //opposite
					list[list_len - 1] = rec._length;
					rec._width -= rec._length;
				}
			
		}
		list_len++;
		void* temp = realloc(list, sizeof(int)*list_len);
		if (!temp) {
			free(list);
			exit(1);
		}
		list = (size_t*)temp;
		list[list_len-1] = rec._length; // this is final case when both sides are equal. Choice arbitrary
		for (int i = 0; i < list_len; i++) {
			printf("%d ", list[i]);
		}
	}
	else {
		printf("Ilegal rec");
	}
}
/*
creates rectngle
in: len, width
out: rec
*/
rectangle init(size_t len, size_t width)
{
	rectangle _new;
	if (len > 0 && width > 0) {
		_new._length = len;
		_new._width = width;
	}
	else {
		printf("Invalid Init");
	}
	return _new;
}
/*
checks is valid rec
in: rec
out: 1 if yes 0 if not
*/
int check_init(rectangle check)
{
	if (check._length && check._width) {
		return 1;
	}
	return 0;
}
