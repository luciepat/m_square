#pragma once
#include <stdio.h>

typedef struct rectangle {
	size_t _length;
	size_t _width;
} rectangle;

size_t* squares(rectangle rec);
rectangle init(size_t len, size_t width);
int check_init(rectangle check);